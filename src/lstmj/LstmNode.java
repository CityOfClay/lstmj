package lstmj;

/**
 *
 * @author Ascari Gutierrez Hermosillo <ascari.gtz@gmail.com>
 */
public class LstmNode {
  public LstmState state;// one state per LstmNode, buffers each timestep
  public LstmParam param;// only one param instance globally, network weights
  double[] xc = null;
  double[] s_prev = null;
  double[] h_prev = null;

  public LstmNode(LstmParam lstm_param) {
    // store reference to parameters and to activations
    this.param = lstm_param;
    this.state = new LstmState(lstm_param.mem_cell_ct);
    this.xc = null;// non-recurrent input concatenated with recurrent input
  }
  public void bottom_data_is(double[] x) {// involved in forward fire
    // if this is the first lstm node in the network
    s_prev = Globals.zeros_like(this.state.s);
    h_prev = Globals.zeros_like(this.state.h);
    bottom_data_is(x, s_prev, h_prev);//    bottom_data_is(x, null, null);
  }
  public void bottom_data_is(double[] x, double[] s_prev, double[] h_prev) {// involved in forward fire
    // save data for use in backprop
    this.s_prev = s_prev;// looks like s is the memory cell
    this.h_prev = h_prev;

    // concatenate x(t) and h(t-1)
    double[] xcl = Globals.hstack(x, h_prev);// xc local

    //this.state.g = Globals.tanh(mult_through(this.param.wg, xcl, this.param.bg));// most likely feeds into loop
    this.state.g = Globals.tanh(Globals.add(Globals.dot(this.param.wg, xcl), this.param.bg));// most likely feeds into loop
    this.state.i = Globals.sigmoid(Globals.add(Globals.dot(this.param.wi, xcl), this.param.bi));
    this.state.f = Globals.sigmoid(Globals.add(Globals.dot(this.param.wf, xcl), this.param.bf));
    this.state.o = Globals.sigmoid(Globals.add(Globals.dot(this.param.wo, xcl), this.param.bo));// output regulator?
    if (true) {
      double[] val0 = Globals.multiply(this.s_prev, this.state.f);// is f the forgetter?
      double[] val1 = Globals.multiply(this.state.g, this.state.i);// i is regulating g here
      this.state.s = Globals.add(val0, val1);
    } else {
      this.state.s = Globals.add(Globals.multiply(this.state.g, this.state.i), Globals.multiply(s_prev, this.state.f));
    }
    if (false) {
      this.state.h = Globals.multiply(this.state.s, this.state.o);// original 
    } else {
      this.state.h = Globals.multiply(Globals.tanh(this.state.s), this.state.o);// like diagram
    }

    this.xc = xcl;
  }
  public void top_diff_is(double[] top_diff_h, double[] top_diff_s) {// involved in backprop/training
    // notice that top_diff_s is carried along the constant error carousel
//    double[] ds = Globals.add(Globals.multiply(this.state.o, top_diff_h), top_diff_s);// original
    double[] OxH = Globals.multiply(this.state.o, top_diff_h);
    double[] ds = Globals.add(OxH, top_diff_s);

    {
//    double[] _do = Globals.multiply(this.state.s, top_diff_h); // original
//    double[] di = Globals.multiply(this.state.g, ds);
//    double[] dg = Globals.multiply(this.state.i, ds);
//    double[] df = Globals.multiply(this.s_prev, ds);
    }
    double[] di = Globals.multiply(this.state.g, ds);
    double[] df = Globals.multiply(this.s_prev, ds);
    double[] _do = Globals.multiply(this.state.s, top_diff_h);
    double[] dg = Globals.multiply(this.state.i, ds);

    // diffs w.r.t. vector inside sigma / tanh function
    double[] di_input = Globals.multiply(Globals.sigmoid_derivative(this.state.i), di);
    double[] df_input = Globals.multiply(Globals.sigmoid_derivative(this.state.f), df);
    double[] do_input = Globals.multiply(Globals.sigmoid_derivative(this.state.o), _do);
    double[] dg_input = Globals.multiply(Globals.tanh_derivative(this.state.g), dg);

    // diffs w.r.t. inputs
    this.param.wi_diff = Globals.add(this.param.wi_diff, Globals.outer(di_input, this.xc));// xc is experience bus
    this.param.wf_diff = Globals.add(this.param.wf_diff, Globals.outer(df_input, this.xc));
    this.param.wo_diff = Globals.add(this.param.wo_diff, Globals.outer(do_input, this.xc));
    this.param.wg_diff = Globals.add(this.param.wg_diff, Globals.outer(dg_input, this.xc));

    // What are b* and b*_diff for?  They are the curve heights!!! Like an extra dimension. 
    this.param.bi_diff = Globals.add(this.param.bi_diff, di_input);
    this.param.bf_diff = Globals.add(this.param.bf_diff, df_input);
    this.param.bo_diff = Globals.add(this.param.bo_diff, do_input);
    this.param.bg_diff = Globals.add(this.param.bg_diff, dg_input);

    // compute bottom diff
    double[] dxc = Globals.zeros_like(this.xc);
    // This below multiplies each *_input vector backward through its matrix, and adds all the results into one vector.
    dxc = Globals.add(dxc, Globals.dot(Globals.transpose(this.param.wi), di_input));// transpose is flip matrix along diagonal
    dxc = Globals.add(dxc, Globals.dot(Globals.transpose(this.param.wf), df_input));
    dxc = Globals.add(dxc, Globals.dot(Globals.transpose(this.param.wo), do_input));
    dxc = Globals.add(dxc, Globals.dot(Globals.transpose(this.param.wg), dg_input));

    // Below the dxc sum vector is then stripped of its input part, and the feedback part is fed backward through the h loop. 
    // save bottom diffs
    this.state.bottom_diff_s = Globals.multiply(ds, this.state.f);
    this.state.bottom_diff_h = Globals.slice(dxc, this.param.x_dim);// x_dim is 50, or size of input. we are selecting only feedback signal.
  }
  public double[] mult_through(double[][] wgtmx, double[] xcl, double[] height) {
    return Globals.add(Globals.dot(wgtmx, xcl), height);
  }
  public String print_me() {
    double val = this.state.h[0];// h[0] seems to be the predicted houtput, while all h is the feedback vector.
    StringBuilder sb = new StringBuilder();
    sb.append(val + ", ");// to do: format this
    return sb.toString();// toFixed(5);
  }
}
