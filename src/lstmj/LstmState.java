package lstmj;

/**
 *
 * @author Ascari Gutierrez Hermosillo <ascari.gtz@gmail.com>
 */
public class LstmState {
  double[] g, i, f, o, s, h;// o is output regulator?
  double[] bottom_diff_h, bottom_diff_s;
  public LstmState(int mem_cell_ct) {
    this.g = new double[mem_cell_ct];
    this.i = new double[mem_cell_ct];
    this.f = new double[mem_cell_ct];
    this.o = new double[mem_cell_ct];
    this.s = new double[mem_cell_ct];
    this.h = new double[mem_cell_ct];
    this.bottom_diff_h = Globals.zeros_like(this.h);
    this.bottom_diff_s = Globals.zeros_like(this.s);
  }
}
