package lstmj;

import static java.lang.System.exit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Ascari Gutierrez Hermosillo <ascari.gtz@gmail.com>
 */
public class LstmNetwork {
  LstmParam lstm_param;
  ToyLossLayer loss_layer;
  ArrayList<LstmNode> lstm_node_list;
  int x_counter;//  ArrayList<Double> x_list;

  LstmNetwork(LstmParam lstm_param) {
    this.lstm_param = lstm_param;
    this.lstm_node_list = new ArrayList<LstmNode>();
    // input sequence
    this.x_counter = 0;// this.x_list = new ArrayList<Double>();
  }
  public double y_list_is(double[] y_list, ToyLossLayer loss_layer) {// net_backward
    /*
     Updates diffs by setting target sequence 
     with corresponding loss layer. 
     Will *NOT* update parameters.  To update parameters,
     call this.lstm_param.apply_diff()
     */
    if (y_list.length != this.x_counter) {//this.x_list.size()) {
      System.out.println("Crasho!");
      exit(1);
    }

    int idx = this.x_counter - 1;//this.x_list.size() - 1;
    // first node only gets diffs from label ...
    double loss = loss_layer.loss(this.lstm_node_list.get(idx).state.h, y_list[idx]);
    double[] diff_h = loss_layer.bottom_diff(this.lstm_node_list.get(idx).state.h, y_list[idx]);// returns [number,0,0,0,0,0,..., 0.0]
    // here s is not affecting loss due to h(t+1), hence we set equal to zero;
    double[] diff_s = Globals.zeros(this.lstm_param.mem_cell_ct);
    this.lstm_node_list.get(idx).top_diff_is(diff_h, diff_s);
    idx -= 1;

    // ... following nodes also get diffs from next nodes, hence we add diffs to diff_h
    // we also propagate error along constant error carousel using diff_s
    while (idx >= 0) {
      loss += loss_layer.loss(this.lstm_node_list.get(idx).state.h, y_list[idx]);
      diff_h = loss_layer.bottom_diff(this.lstm_node_list.get(idx).state.h, y_list[idx]);// returns [number,0,0,0,0,0,..., 0.0]
      diff_h = Globals.add(diff_h, this.lstm_node_list.get(idx + 1).state.bottom_diff_h);

      diff_s = this.lstm_node_list.get(idx + 1).state.bottom_diff_s;
      this.lstm_node_list.get(idx).top_diff_is(diff_h, diff_s);
      idx -= 1;
    }
    return loss;
  }

  public void x_list_clear() {
    this.x_counter = 0;//this.x_list = new ArrayList<Double>();
  }
  
  public void x_list_add(double[] x) { // net_forward
//    System.out.println("x_list.size:" + this.x_list.size() + ", lstm_node_list.size:" + this.lstm_node_list.size());
    // get index of most recent x input
    int idx = this.x_counter;// - 1;//    int idx = this.x_list.size() - 1;
    LstmNode current_node = this.lstm_node_list.get(idx);
    if (idx == 0) {// no recurrent inputs yet
      current_node.bottom_data_is(x);// if this is the first lstm node in the network
    } else {
      LstmNode prev_node = this.lstm_node_list.get(idx - 1);
      double[] s_prev = prev_node.state.s;
      double[] h_prev = prev_node.state.h;
      current_node.bottom_data_is(x, s_prev, h_prev);
    }
    this.x_counter++;//List xray = Arrays.asList(x); this.x_list.addAll(xray);// list of vectors
  }
  
  public String print_me() {
    int NumNodes = this.lstm_node_list.size();
    return print_me(NumNodes);
  }
  public String print_me(int NumNodes) {
    StringBuilder sb = new StringBuilder();
    for (int ncnt = 0; ncnt < NumNodes; ncnt++) {
      LstmNode node = this.lstm_node_list.get(ncnt);
      sb.append(node.print_me() + " ");
    }
    return sb.toString();
  }
}
