package lstmj;

import java.util.Random;

/**
 *
 * @author Ascari Gutierrez Hermosillo <ascari.gtz@gmail.com>
 */
public class Globals {
  public static Random rand = new Random();
  public static double random() {
//  rand: np.random,
//  random: (shape) => shape ? nj_random(shape) : Math.random(),
//  seed: (s) => np.random._seed = s; // js doesn't seed its PRNG
    return rand.nextDouble();
  }
  /* ****************************************************** */
  public static double sigmoid_scalar(double value) {
    double OutVal;// sigmoid function in range 0.0 to 1.0. 
    OutVal = 1.0 / (1.0 + Math.exp(-value));
    return OutVal;
  }

  public static double[] sigmoid(double[] values) {// TODO, fill this in
    double[] retval = new double[values.length];
    for (int cnt = 0; cnt < values.length; cnt++) {
      retval[cnt] = sigmoid_scalar(values[cnt]);
    }
    return retval;// return np.ones(x.shape).divide( np.ones(x.shape).add(np.exp(x.multiply(-1))));
  }

  public static double sigmoid_derivative_scalar(double value) {
    double OutVal;// derivative of sigmoid function in range 0.0 to 1.0. 
    OutVal = value * (1.0 - value);// this derivative works on post-fire (already-sigmoided) values. 
    return OutVal;
  }

  public static double[] sigmoid_derivative(double[] values) {// TODO, fill this in
    double[] retval = new double[values.length];
    for (int cnt = 0; cnt < values.length; cnt++) {
      retval[cnt] = sigmoid_derivative_scalar(values[cnt]);
    }
    return retval;// return values.multiply(np.ones(values.shape).subtract(values));
  }

  public static double[] tanh(double[] values) {// TODO, fill this in
    double[] retval = new double[values.length];
    for (int cnt = 0; cnt < values.length; cnt++) {
      retval[cnt] = Math.tanh(values[cnt]);
    }
    return retval;
  }

  public static double tanh_derivative_scalar(double value) {
    double OutVal;// derivative of tanh function in range -1.0 to 1.0. 
    OutVal = 1.0 - value * value;
    return OutVal;//  OutVal = value / Math.sqrt(1.0 + value * value);// does this work too?
  }

  public static double[] tanh_derivative(double[] values) {// TODO, fill this in
    double[] retval = new double[values.length];
    for (int cnt = 0; cnt < values.length; cnt++) {
      retval[cnt] = tanh_derivative_scalar(values[cnt]);
    }
    return retval;
  }

  // createst uniform random array w/ values in [a,b) and shape args
  public static double[] rand_arr(double low, double high, int NumRows) {
    double[] retval = new double[NumRows];
    double range = high - low;
    for (int cnt = 0; cnt < NumRows; cnt++) {
      retval[cnt] = (rand.nextDouble() * range) + low;
    }
    if (false) {
      retval = fillray(retval);
      printray(retval);
      System.out.println();
    }
    return retval;
  }
  // createst uniform random array w/ values in [a,b) and shape args
  public static double[][] rand_arr(double low, double high, int NumRows, int NumCols) {
    double[][] retval = new double[NumRows][NumCols];
    double range = high - low;
    for (int rcnt = 0; rcnt < NumRows; rcnt++) {
      for (int ccnt = 0; ccnt < NumCols; ccnt++) {
        retval[rcnt][ccnt] = (rand.nextDouble() * range) + low;
      }
    }
    if (false) {
      retval = fillray(retval);
      printray(retval);
      System.out.println();
    }
    return retval;
  }

  public static double[][] outer(double[] RowVect, double[] ColVect) {// presumed outer product
    double[][] retval;
    retval = new double[RowVect.length][ColVect.length];
    double product;
    for (int cnt0 = 0; cnt0 < RowVect.length; cnt0++) {
      for (int cnt1 = 0; cnt1 < ColVect.length; cnt1++) {
        product = RowVect[cnt0] * ColVect[cnt1];
        retval[cnt0][cnt1] = product;
      }
    }
    return retval;
  }

  public static double[] dot(double[][] matrix, double[] vector) {// dot product 
    int NumRows = matrix.length;// really just multiplying vector through matrix
    int NumCols = vector.length;
    double[] retvector;
    retvector = new double[NumRows];
    double product, rowsumprod;
    for (int RowCnt = 0; RowCnt < NumRows; RowCnt++) {// row count
      rowsumprod = 0;
      for (int ColCnt = 0; ColCnt < NumCols; ColCnt++) {// column count
        product = matrix[RowCnt][ColCnt] * vector[ColCnt];
        rowsumprod += product;
      }
      retvector[RowCnt] = rowsumprod;
    }
    return retvector;
  }

  public static double[][] transpose(double[][] matrix) {// Flip matrix along diagonal
    int NumRows = matrix.length;
    int NumCols = matrix[0].length;
    double[][] retval = new double[NumCols][NumRows];
    for (int cnt0 = 0; cnt0 < NumRows; cnt0++) {// row count
      for (int cnt1 = 0; cnt1 < NumCols; cnt1++) {// column count
        retval[cnt1][cnt0] = matrix[cnt0][cnt1];
      }
    }
    return retval;
  }

  public static double[] concat(double[] a, double[] b) {// append two 1D arrays
    double[] retval = new double[a.length + b.length];
    System.arraycopy(a, 0, retval, 0, a.length);
    System.arraycopy(b, 0, retval, a.length, b.length);
    return retval;
  }

  public static double[][] concat(double[][] a, double[][] b) {// append two 2D arrays
    int NumCols = a[0].length;// WARNING UNTESTED
    double[][] retval = new double[a.length + b.length][];//[NumCols]; ?
    System.arraycopy(a, 0, retval, 0, a.length);
    System.arraycopy(b, 0, retval, a.length, b.length);// will this work on 2D arrays?
    return retval;
  }

  public static double[] hstack(double[] a, double[] b) {// horizontal stacking of arrays, 1d is append
    double[] retval = new double[a.length + b.length];
    System.arraycopy(a, 0, retval, 0, a.length);
    System.arraycopy(b, 0, retval, a.length, b.length);
    return retval;
  }

  public static double[][] hstack(double[][] a, double[][] b) {// horizontal stacking of arrays, 2d is ?
    int arows = a.length;
    int acols = a[0].length;
    int brows = b.length;
    int bcols = b[0].length;
    double[][] retval = new double[arows][acols + bcols];
    for (int rcnt = 0; rcnt < arows; rcnt++) {
      System.arraycopy(a[rcnt], 0, retval[rcnt], 0, acols);
      System.arraycopy(b[rcnt], 0, retval[rcnt], acols, bcols);// maybe works?  untested
    }
    return retval;
//    for (int i = 0; i < src.length; i++) {
//      System.arraycopy(src[i], 0, dest[i], 0, src[0].length);
//    }
  }

  public static double[] slice(double[] vect, int chop) {
    int len = vect.length - chop;// Return a copy of vect from chop index to end.
    double[] retval = new double[len];
    System.arraycopy(vect, chop, retval, 0, len);
    return retval;
  }

  public static double[] zeros_like(double[] x) {
    double[] retval = new double[x.length];
    return retval;//return nj.zeros(x.shape);
  }
  public static double[][] zeros_like(double[][] val0) {
    int arows = val0.length;
    int acols = val0[0].length;
    double[][] retval = new double[arows][acols];
    return retval;
  }

  public static double[] zeros(int rows) {
    double[] retval = new double[rows];
    return retval;//return nj.zeros(x.shape);
  }

  public static double[][] zeros(int rows, int cols) {
    double[][] retval = new double[rows][cols];
    return retval;// return nj.zeros(x.shape);
  }

  public static double[] add(double[] val0, double[] val1) {
    if (val0.length != val1.length) {
      System.out.println("Length mismatch in add!");
    }
    double[] retval = new double[val0.length];
    for (int cnt0 = 0; cnt0 < val0.length; cnt0++) {
      retval[cnt0] = val0[cnt0] + val1[cnt0];
    }
    return retval;
  }
  public static double[][] add(double[][] val0, double[][] val1) {
    int rows0 = val0.length;
    int cols0 = val0[0].length;
    int rows1 = val1.length;
    int cols1 = val1[0].length;
    if (rows0 != rows1) {
      System.out.println("Row mismatch in add!");
    }
    if (cols0 != cols1) {
      System.out.println("Column mismatch in add!");
    }
    double[][] retval = new double[rows0][cols0];
    for (int cnt0 = 0; cnt0 < rows0; cnt0++) {
      for (int cnt1 = 0; cnt1 < cols0; cnt1++) {
        retval[cnt0][cnt1] = val0[cnt0][cnt1] + val1[cnt0][cnt1];
      }
    }
    return retval;
  }

  public static double[] subtract(double[] val0, double[] val1) {
    double[] retval = new double[val0.length];
    for (int cnt0 = 0; cnt0 < val0.length; cnt0++) {
      retval[cnt0] = val0[cnt0] - val1[cnt0];
    }
    return retval;
  }

  public static double[][] subtract(double[][] val0, double[][] val1) {
    int arows = val0.length;
    int acols = val0[0].length;
    int brows = val1.length;
    int bcols = val1[0].length;
    double[][] retval = new double[arows][acols];
    for (int cnt0 = 0; cnt0 < arows; cnt0++) {
      for (int cnt1 = 0; cnt1 < acols; cnt1++) {
        retval[cnt0][cnt1] = val0[cnt0][cnt1] - val1[cnt0][cnt1];
      }
    }
    return retval;
  }

  public static double[] multiply(double[] val0, double val1) {
    double[] retval = new double[val0.length];
    for (int cnt0 = 0; cnt0 < val0.length; cnt0++) {
      retval[cnt0] = val0[cnt0] * val1;
    }
    return retval;
  }
  public static double[][] multiply(double[][] val0, double val1) {//   WARNING NOT FUNCTIONAL YET
    int arows = val0.length;
    int acols = val0[0].length;
    double[][] retval = new double[arows][acols];
    for (int cnt0 = 0; cnt0 < arows; cnt0++) {
      for (int cnt1 = 0; cnt1 < acols; cnt1++) {
        retval[cnt0][cnt1] = val0[cnt0][cnt1] * val1;
      }
    }
    return retval;
  }

  public static double[] multiply(double[] val0, double[] val1) {
    if (val0.length != val1.length) {
      System.out.println();
    }
    double[] retval = new double[val0.length];
    for (int cnt0 = 0; cnt0 < val0.length; cnt0++) {
      try {
        retval[cnt0] = val0[cnt0] * val1[cnt0];
      } catch (Exception ex) {
        System.out.println();
      }
    }
    return retval;
  }
  public static double[][] multiply(double[][] val0, double[][] val1) {//   WARNING NOT FUNCTIONAL YET
    int arows = val0.length;
    int acols = val0[0].length;
    int brows = val1.length;
    int bcols = val1[0].length;
    double[][] retval = new double[arows][acols];
    for (int cnt0 = 0; cnt0 < arows; cnt0++) {
      for (int cnt1 = 0; cnt1 < acols; cnt1++) {
        retval[cnt0][cnt1] = val0[cnt0][cnt1] * val1[cnt0][cnt1];
      }
    }
    return retval;
  }
  public static void printray(double[] ray) {
    int arows = ray.length;
    double value = 0.0;
    System.out.print("[");
    for (int cnt0 = 0; cnt0 < arows; cnt0++) {
      value = ray[cnt0];
      System.out.print(value + ", ");
    }
    System.out.print("]");
  }

  public static double[][] printray(double[][] ray) {
    int arows = ray.length;
    int acols = ray[0].length;
    double value = 0.0;
    System.out.print("[");
    for (int cnt0 = 0; cnt0 < arows; cnt0++) {
      System.out.print("[");
      for (int cnt1 = 0; cnt1 < acols; cnt1++) {
        value = ray[cnt0][cnt1];
        System.out.print(value + ", ");
      }
      System.out.print("], \n");
    }
    System.out.print("]");
    return ray;
  }

  public static double[] fillray(double[] ray) {
    int arows = ray.length;
    double amp = 0.1;
    double value = 0.0;
    double raylen = arows;
    double factor = amp / raylen;
    double offset = -amp * 0.5;
    for (int cnt0 = 0; cnt0 < arows; cnt0++) {
      value = value + 1.0;
      ray[cnt0] = (value * factor) + offset;
    }
    return ray;
  }
  public static double[][] fillray(double[][] ray) {
    int arows = ray.length;
    int acols = ray[0].length;
    double amp = 0.1;
    double value = 0.0;
    double raylen = arows * acols;
    double factor = amp / raylen;
    double offset = -amp * 0.5;
    for (int cnt0 = 0; cnt0 < arows; cnt0++) {
      for (int cnt1 = 0; cnt1 < acols; cnt1++) {
        value = value + 1.0;
        ray[cnt0][cnt1] = (value * factor) + offset;
      }
    }
    return ray;
  }
}

/*
 # Python iterators from https://stackoverflow.com/questions/29478748/modify-actual-element-value-in-array-numpy
 def fillray(arr):
 amp = 0.1
 # amp = 0.2
 # amp = 0.09
 value = 0.0
 raylen = arr.size
 factor = amp / raylen
 offset = -amp * 0.5
 for x in np.nditer(arr, op_flags=['readwrite']):
 . value = value + 1.0
 . x[...] = (value * factor) + offset
 . print(arr)
 return arr

 */
