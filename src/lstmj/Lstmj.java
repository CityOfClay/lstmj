package lstmj;

/**
 *
 * @author Ascari Gutierrez Hermosillo <ascari.gtz@gmail.com>
 */
public class Lstmj {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    example_0();
  }

  // Python's range() equivalent
  //const range = (a) => [...Array(a).keys()];
  public static void example_0() {
    // learns to repeat simple sequence from random inputs
    //Globals.random.seed(0);
    // parameters for input data dimension and lstm cell count
//    int mem_cell_ct = 40;
//    int x_dim = 20;
    int mem_cell_ct = 100;
    int x_dim = 50;

    LstmParam lstm_param = new LstmParam(mem_cell_ct, x_dim);
    LstmNetwork lstm_net = new LstmNetwork(lstm_param);
    double[] y_list = {-0.5, 0.2, 0.1, -0.5};
//    double[] y_list = {-0.5, 0.1, -0.5, -0.5, -0.5, -0.5};
    double[][] input_val_arr = Globals.rand_arr(0.0, 1.0, y_list.length, x_dim);// double[][] input_val_arr = y_list.map(x =  > np.random.random(x_dim));
//    input_val_arr = Globals.fillray(input_val_arr);

    // create all nodes and add to network
    for (int cnt = 0; cnt < y_list.length; cnt++) {// need to add new lstm node, create new state mem
      lstm_net.lstm_node_list.add(new LstmNode(lstm_param));
    }

    int MaxGens = 100;
    for (int cur_iter_cnt = 0; cur_iter_cnt < MaxGens; cur_iter_cnt++) {
      for (int ind_cnt = 0; ind_cnt < y_list.length; ind_cnt++) {
        lstm_net.x_list_add(input_val_arr[ind_cnt]);
      }
      double loss = lstm_net.y_list_is(y_list, new ToyLossLayer());

      System.out.print("iter:" + cur_iter_cnt + ", ");
      System.out.print("y_pred = [");
      System.out.print(lstm_net.print_me(y_list.length));// toFixed(5);
      System.out.print("]");
      System.out.print("loss:" + loss + ", ");
      System.out.println();
//      console.log(
//        'iter',
//        cur_iter,
//        ':',
//        'y_pred = [',
//        range(y_list.length).map(ind => {
//          const node = lstm_net.lstm_node_list[ind];
//          return node.state.h.get(0).toFixed(5);
//        }).join(','),
//        ']',
//        loss
//      );
      lstm_param.apply_diff(0.1);
      lstm_net.x_list_clear();
    }
  }

}
// ***************************************

/**
 * !
 * Written by Ascari Gutierrez Hermosillo <ascari.gtz@gmail.com>
 * Enjoy!
 * 
 * also look at this http://bl.ocks.org/dribnet/cd6ee08b7658e5c744307b44b438221f
 * 
 var update = function(x_, s) {
    var x = nj.zeros(input_size);
    x.set(0, x_[0]/scale_factor);
    x.set(1, x_[1]/scale_factor);
    x.set(2, x_[2]);
    var h = s[0];
    var c = s[1];
    var concat = nj.concatenate([x, h]);
    var hidden = nj.dot(concat, W_full);
    hidden = nj.add(hidden, bias);

    var i=nj.sigmoid(hidden.slice([0*num_units, 1*num_units]));
    var g=nj.tanh(hidden.slice([1*num_units, 2*num_units]));
    var f=nj.sigmoid(nj.add(hidden.slice([2*num_units, 3*num_units]), forget_bias));
    var o=nj.sigmoid(hidden.slice([3*num_units, 4*num_units]));

    var new_c = nj.add(nj.multiply(c, f), nj.multiply(g, i));
    var new_h = nj.multiply(nj.tanh(new_c), o);

    return [new_h, new_c];
  }

 */
