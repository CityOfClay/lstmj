package lstmj;

/**
 *
 * @author Ascari Gutierrez Hermosillo <ascari.gtz@gmail.com>
 */
public class LstmParam {// all of the connections and weights
  public int mem_cell_ct, x_dim;

  double[][] wg, wi, wf, wo;// all connection weights
  double[][] wg_diff, wi_diff, wf_diff, wo_diff;// for learning
  double[] bg, bi, bf, bo;// bias terms (like curve height?)
  double[] bg_diff, bi_diff, bf_diff, bo_diff;// for learning

  public LstmParam(int mem_cell_ct, int x_dim) {
    this.mem_cell_ct = mem_cell_ct;// matrix output size
    this.x_dim = x_dim;
    int concat_len = x_dim + mem_cell_ct;// matrix input size
    // weight matrices
    this.wg = Globals.rand_arr(-0.1, 0.1, mem_cell_ct, concat_len);
    this.wi = Globals.rand_arr(-0.1, 0.1, mem_cell_ct, concat_len);
    this.wf = Globals.rand_arr(-0.1, 0.1, mem_cell_ct, concat_len);
    this.wo = Globals.rand_arr(-0.1, 0.1, mem_cell_ct, concat_len);
    // bias terms
    this.bg = Globals.rand_arr(-0.1, 0.1, mem_cell_ct);
    this.bi = Globals.rand_arr(-0.1, 0.1, mem_cell_ct);
    this.bf = Globals.rand_arr(-0.1, 0.1, mem_cell_ct);
    this.bo = Globals.rand_arr(-0.1, 0.1, mem_cell_ct);
    // diffs (derivative of loss function w.r.t. all parameters)
    this.wg_diff = Globals.zeros(mem_cell_ct, concat_len);
    this.wi_diff = Globals.zeros(mem_cell_ct, concat_len);
    this.wf_diff = Globals.zeros(mem_cell_ct, concat_len);
    this.wo_diff = Globals.zeros(mem_cell_ct, concat_len);
    this.bg_diff = Globals.zeros(mem_cell_ct);
    this.bi_diff = Globals.zeros(mem_cell_ct);
    this.bf_diff = Globals.zeros(mem_cell_ct);
    this.bo_diff = Globals.zeros(mem_cell_ct);
  }
  public void apply_diff(double lr) {// lr = 1 default
    this.wg = Globals.subtract(this.wg, Globals.multiply(this.wg_diff, lr));
    this.wi = Globals.subtract(this.wi, Globals.multiply(this.wi_diff, lr));
    this.wf = Globals.subtract(this.wf, Globals.multiply(this.wf_diff, lr));
    this.wo = Globals.subtract(this.wo, Globals.multiply(this.wo_diff, lr));
    this.bg = Globals.subtract(this.bg, Globals.multiply(this.bg_diff, lr));
    this.bi = Globals.subtract(this.bi, Globals.multiply(this.bi_diff, lr));
    this.bf = Globals.subtract(this.bf, Globals.multiply(this.bf_diff, lr));
    this.bo = Globals.subtract(this.bo, Globals.multiply(this.bo_diff, lr));

    // reset diffs to zero
    this.wg_diff = Globals.zeros_like(this.wg);
    this.wi_diff = Globals.zeros_like(this.wi);
    this.wf_diff = Globals.zeros_like(this.wf);
    this.wo_diff = Globals.zeros_like(this.wo);
    this.bg_diff = Globals.zeros_like(this.bg);
    this.bi_diff = Globals.zeros_like(this.bi);
    this.bf_diff = Globals.zeros_like(this.bf);
    this.bo_diff = Globals.zeros_like(this.bo);
  }
}
