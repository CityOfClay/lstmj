package lstmj;

/**
 *
 * @author Ascari Gutierrez Hermosillo <ascari.gtz@gmail.com>
 */
public class ToyLossLayer {
  /*
   Computes square loss with first element of hidden layer array.
   */
  public double loss(double[] pred, double label) {
    double delta = pred[0] - label;
    return Math.pow(delta, 2);//return Math.pow((pred.get(0) - label), 2);
  }
  public double[] bottom_diff(double[] pred, double label) {
    double[] diff = Globals.zeros_like(pred);
    double delta = pred[0] - label;
    diff[0] = 2.0 * delta;//diff.set(0, 2.0 * (pred.get(0) - label));
    return diff;
  }

}
